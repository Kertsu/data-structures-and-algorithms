steps = 1

def bubble_sort(list):
    for i in range(len(list) - 1, 0, -1):
        for j in range(i):
            if list[j] > list[j + 1]:
                temp = list[j]
                list[j] = list[j + 1]
                list[j + 1] = temp
                globals()["steps"] = steps + 1


values = input("Enter any numbers that are comma-separated (no spaces):\n")
list = values.split(",")

for i in range(0, len(list)):
    list[i] = int(list[i])


bubble_sort(list)
print(list)
print("Steps taken: " + str(steps))
