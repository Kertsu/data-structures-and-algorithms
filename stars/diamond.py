def main(size):

    i = 0
    while i < size:
        j = 0
        while j < size - i:
            print(' ', end = '')
            j += 1
        k = 0
        while k < i + 1 :
            print('*', end = '')
            k += 1
        l = 0
        while l < i:
            print('*', end = '')
            l += 1
        i += 1
        print()

    m = 0 
    while m < size:
        n = 0
        while n < m + 2:
            print(' ', end = '')
            n += 1
        o = 0
        while o < size - 1 - m:
            print('*', end = '')
            o += 1
        p = 0
        while p < o - 1:
            print('*', end = '')
            p += 1
        m += 1 
        print()

main(5)